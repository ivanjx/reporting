﻿using Reporting.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.ViewModels
{
    public class ProductViewModel
    {
        public Product Model
        {
            get;
            private set;
        }

        public string ID
        {
            get
            {
                return Model.ID;
            }
        }

        public string Name
        {
            get
            {
                return Model.Name;
            }
        }

        public double Quantity
        {
            get
            {
                return Model.Quantity;
            }
        }

        public string PriceString
        {
            get
            {
                return Model.Price.ToString("C");
            }
        }


        public ProductViewModel(Product model)
        {
            Model = model;
        }

        public override bool Equals(object obj)
        {
            ProductViewModel comp;
            if ((comp = obj as ProductViewModel) != null)
            {
                return Model.Equals(comp.Model);
            }

            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
