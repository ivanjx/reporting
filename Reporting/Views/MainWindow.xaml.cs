﻿using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;
using Microsoft.Reporting.WinForms;
using Reporting.Models;
using Reporting.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Reporting.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        ObservableCollection<ProductViewModel> m_products;


        public MainWindow()
        {
            InitializeComponent();
            m_products = new ObservableCollection<ProductViewModel>();
            lstItems.ItemsSource = m_products;
        }

        private void btnOK_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtID_AddProduct.Text))
                {
                    throw new Exception("ID is null");
                }

                if (string.IsNullOrEmpty(txtProductName_AddProduct.Text))
                {
                    throw new Exception("Product name is null");
                }

                if (string.IsNullOrEmpty(txtPrice_AddProduct.Text))
                {
                    throw new Exception("Price is null");
                }

                if (string.IsNullOrEmpty(txtQuantity_AddProduct.Text))
                {
                    throw new Exception("Quantity is null");
                }

                Product p = new Product();
                p.ID = txtID_AddProduct.Text;
                p.Name = txtProductName_AddProduct.Text;
                p.Quantity = double.Parse(txtQuantity_AddProduct.Text);
                p.Price = decimal.Parse(txtPrice_AddProduct.Text);

                ProductViewModel vm = new ProductViewModel(p);
                m_products.Add(vm);

                btnCancel_AddProduct_Click(null, null);
            }
            catch (Exception ex)
            {
                this.ShowMessageAsync("Error", "Unable to add product.\nDetails: " + ex.Message);
            }
        }

        private void btnCancel_AddProduct_Click(object sender, RoutedEventArgs e)
        {
            txtID_AddProduct.Text = string.Empty;
            txtPrice_AddProduct.Text = string.Empty;
            txtProductName_AddProduct.Text = string.Empty;
            txtQuantity_AddProduct.Text = string.Empty;

            gridAddProduct.Visibility = Visibility.Hidden;
        }

        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            gridAddProduct.Visibility = Visibility.Visible;
        }

        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            ProductViewModel selitem;
            if ((selitem = lstItems.SelectedItem as ProductViewModel) != null)
            {
                m_products.Remove(selitem);
            }
        }

        private void btnPrint_Click(object sender, RoutedEventArgs e)
        {
            // Generating data source from the list.
            List<Product> products = new List<Product>();

            for (int i = 0; i < m_products.Count; ++i)
            {
                products.Add(m_products[i].Model);
            }

            // Filling the data set.
            ReportDataSource dataSource = new ReportDataSource("ProductDataSet", products);

            // Showing the print dialog.
            PrintWindow printWindow = new PrintWindow(dataSource, "Reporting.Resources.ReportTemplate.ReportTemplate.rdlc");
            printWindow.ShowDialog();
        }
    }
}
